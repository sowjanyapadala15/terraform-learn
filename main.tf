# Configure the AWS Provider
provider "aws" {
  region = "ca-central-1"
  
}

/*
variable "vpc-cidr-block" {
    description = "dev-vpc-cidr-block"
    #default= "10.0.30.0/24"
  
}
*/

variable "cidr-blocks" {
    description = "cidr-blocks"
    #type= list(string)
    type = list(object({
    cidr_block = string
    name = string
  }))
}

variable "avail_zone" {
  
}

/*variable "environment" {
    description = "deployment env"
  
}*/

resource "aws_vpc" "development-vpc" {
  cidr_block = var.cidr-blocks[0].cidr_block
  tags = {
    Name = var.cidr-blocks[0].name
    vpc_env = "dev"
  }
}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id     = aws_vpc.development-vpc.id
  cidr_block = var.cidr-blocks[1].cidr_block
  availability_zone = var.avail_zone
  tags = {
    Name = var.cidr-blocks[1].name
  }
}

data "aws_vpc" "existing_vpc" {
  default = true
}

resource "aws_subnet" "dev-subnet-2" {
    vpc_id            = data.aws_vpc.existing_vpc.id
    availability_zone = "ca-central-1a"
    cidr_block        = "172.31.48.0/20"
    tags = {
      Name = "subnet-2-dev"
    }
}

output "dev-vpc-id" {
    value = aws_vpc.development-vpc.id 
}

output "dev-subnet-id" {
    value = aws_subnet.dev-subnet-1.id
}
  
